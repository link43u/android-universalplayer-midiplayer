package com.example.android.uamp.media;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"NOW_PLAYING_CHANNEL", "", "NOW_PLAYING_NOTIFICATION", "", "common_debug"})
public final class NotificationBuilderKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NOW_PLAYING_CHANNEL = "com.example.android.uamp.media.NOW_PLAYING";
    public static final int NOW_PLAYING_NOTIFICATION = 45881;
}